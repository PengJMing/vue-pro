import Vue from 'vue'
import Router from 'vue-router'
import { Message } from 'element-ui' // 需要设置提示信息所以导入
// 路由懒加载
const login = () => import('../component/login/login.vue') // 登录组件
const home = () => import('../component/home/home.vue')//  主页组件
const users = () => import('../component/users/users.vue') // 用户组件
const rights = () => import('../component/rights/rights.vue')// 权限列表组件
const roles = () => import('../component/roles/roles.vue')// 角色列表组件
const categories = () => import('../component/categories/categories.vue') // 商品分类
const goods = () => import('../component/goods/goods.vue') // 商品分类
const add = () => import('../component/add/add.vue') // 商品分类
const orders = () => import('../component/orders/orders.vue') // 订单管理
const reports = () => import('../component/reports/reports.vue') // 数据报表
Vue.use(Router)

let router = new Router({
  routes: [
    {path: '/', redirect: '/home'},
    {path: '/login', component: login}, //  登录页面路由
    // 主页路由
    {
      path: '/home',
      component: home,
      children: [
        {path: 'users', component: users},
        {path: 'rights', component: rights},
        {path: 'roles', component: roles},
        {path: 'categories', component: categories},
        {path: 'goods', component: goods},
        {path: '/goods/add', component: add},
        {path: 'orders', component: orders},
        {path: 'reports', component: reports}
      ]}
  ]
})
// 设置导航守卫,如果不是去往login的,都需要验证token
router.beforeEach((to, from, next) => {
  if (to.path !== '/login') {
    if (!localStorage.getItem('token')) {
      Message({
        type: 'info',
        message: '请先登录!'
      })
      router.push('/login')
      return
    }
  }
  next()
})

export default router
