import axios from 'axios'

let myaxios = {}

myaxios.install = function (Vue) {
  // 设置axios的过滤器
  axios.interceptors.request.use(function (config) {
    // 当路径不是login登录时,就添加公共报头token令牌
    if (config.url !== '"http://localhost:8888/api/private/v1/login"') {
      config.headers.common['Authorization'] = localStorage.getItem('token')
    }
    return config
  })
  axios.defaults.baseURL = 'http://localhost:8888/api/private/v1/'
  Vue.prototype.$http = axios
}

export default myaxios
