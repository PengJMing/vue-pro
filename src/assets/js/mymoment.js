import moment from 'moment'

let mymoment = {}

mymoment.install = function (Vue) {
  Vue.filter('mymoment', function (msg, str) {
    return moment(msg).format(str)
  })
}

export default mymoment
