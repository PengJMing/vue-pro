// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import myaxios from './assets/js/myaxios' //  自定义的ajax插件
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import './assets/css/index.css' //  全局样式
import mymoment from './assets/js/mymoment.js'
import ElTreeGrid from 'element-tree-grid' // element表格树形插件,解决当前element版本表格树形的箭头无法生成问题
import VueQuillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

Vue.component(ElTreeGrid.name, ElTreeGrid) // 注册为全局
Vue.use(VueQuillEditor)
Vue.use(mymoment) // 使用自定义时间格式插件
Vue.use(myaxios) // 使用全局的ajax
Vue.use(ElementUI)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {
    App
  },
  template: '<App/>'
})
